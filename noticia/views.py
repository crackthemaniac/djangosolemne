from django.shortcuts import render
from noticia.models import Posteo
from django.http import HttpResponse, Http404


def inicio(request):
	template_nombre = 'index.html'
	data = {}
	#SELECT * FROM app WHERE status = 1
	data['posts'] = Posteo.objects.filter(estado=True).order_by('-fecha_publicacion')
	data['destacada'] = Posteo.objects.filter(destacada=True).order_by('-fecha_publicacion')[:1]

	return render(request, template_nombre, data)


def detalle(request, noticia_id):
	try:
		noticia = Posteo.objects.get(pk=noticia_id)
	except Posteo.DoesNotExist:
		raise Http404("Noticia Inexistente")

	template_nombre= 'noticia_detalle.html'
	# data = {}
	return render(request, template_nombre, {'noticia':noticia})

