from django.shortcuts import render

def base(request):
	template_name = 'base.html'
	data = {}
	return render(request, template_name, data)
